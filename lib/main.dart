import 'package:flutter/material.dart';

void main() {
  runApp(const flutterlayout());
}

class flutterlayout extends StatelessWidget {
  const flutterlayout({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.lightGreen,
      ),
      home: const MyHomePage(title: 'First Layout'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);



  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body:Container(
        color: Colors.green[100],
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Icon(
                Icons.insert_photo,
                size: 200,
                color: Colors.black45,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                      "Row child 1",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                  ),
                  Text(
                    "Row child 2",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,),
                  ),
                  Text(
                    "Row child 3",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  ],

              ),
              Padding(
                padding: const EdgeInsets.all(30),
                child: Text(
                    "This is Column",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,),
                  ),
              ),
              Padding(
                padding: const EdgeInsets.all(15),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Username',
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15,right: 15,bottom: 30),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Password',
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),
              Row(
                children: [
                  ElevatedButton(
                  onPressed: () {},
                  child: Text('Cancel'),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.lime,
                      onPrimary: Colors.black,
                      padding: EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                      textStyle: TextStyle(fontSize: 20),
                    ),

                    ),
                  ElevatedButton(
                    onPressed: () {},
                    child: Text('Log in'),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.lime,
                      onPrimary: Colors.black,
                      padding: EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                      textStyle: TextStyle(fontSize: 20),
                    ),

                  ),
                ],
              ),
            ],
          ),
        ),
      )
    );
  }
}
